package pcd.lab08.rx;

import java.util.stream.IntStream;
import java.util.stream.LongStream;

import io.reactivex.BackpressureOverflowStrategy;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class Test05_backpressure {

	public static void main(String[] args) throws Exception {

		log("creating hot observable.");

		Flowable<Long> source = genHotStream(100);
		 
		log("subscribing .");

		source
		  .onBackpressureBuffer(5, () -> { log("HELP!"); })
		  // .onBackpressureBuffer(5, () -> { log("HELP!"); }, BackpressureOverflowStrategy.DROP_OLDEST)
		  .observeOn(Schedulers.computation())	
		  .subscribe(Test05_backpressure::compute, Throwable::printStackTrace);
		
		Thread.sleep(1000_000);
	}
	
	private static Flowable<Long> genHotStream(long delay) {
		Flowable<Long> source = Flowable.create(emitter -> {		     
			new Thread(() -> {
				long i = 0;
				while (true){
					try {
						log("emitting: " + i);
						emitter.onNext(i);
						i++;
						if (delay > 0) {
							Thread.sleep(delay);
						}
					} catch (Exception ex){}
				}
			}).start();
		 }, BackpressureStrategy.BUFFER);

		ConnectableFlowable<Long> hotObservable = source.publish();
		hotObservable.connect();
		return hotObservable;		
	}
	
	private static void compute(Long v) {
        try {
            log("compute v: " + v);
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
	
	static private void log(String msg) {
		System.out.println("[" + Thread.currentThread() + "] " + msg);
	}
	

}
