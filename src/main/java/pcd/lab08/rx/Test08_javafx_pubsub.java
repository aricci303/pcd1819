package pcd.lab08.rx;

import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.application.*;
import javafx.geometry.*;
import javafx.stage.Stage;
import javafx.scene.text.*;

import io.reactivex.rxjavafx.observables.JavaFxObservable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.Observable;

import java.net.*;
import java.util.*;

public final class Test08_javafx_pubsub extends Application {

	public void start(Stage stage) throws Exception {

        stage.setTitle("Test");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));

		Scene scene = new Scene(grid, 300, 275);
		stage.setScene(scene);	
		
		Text scenetitle = new Text("Welcome");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		grid.add(scenetitle, 0, 0, 2, 1);
		
		Button refreshButton = new Button("REFRESH");
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn.getChildren().add(refreshButton);
		grid.add(hbBtn, 1, 3);
		
        /*
        btn.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });*/
        
		Text content = new Text("");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		grid.add(content, 1, 5);		
        stage.show();
        
        String url = "https://api.github.com/users";
        		
		JavaFxObservable.actionEventsOf(refreshButton)
			.observeOn(Schedulers.io())
			.flatMap(ev -> Observable.just(getResponse(url)))
			.observeOn(JavaFxScheduler.platform())
			.subscribe(text -> {
				System.out.println(text);
				content.setText(text.substring(0,10)+"...");
			});
						
	}
	
	public static void main(String[] args) {
        launch(args);
    }
	private static String getResponse(String path) {
		try {
			return  new Scanner(new URL(path).openStream(),"UTF-8").next();
		} catch (Exception ex){
			return ex.getMessage();
		}
	}
	
	
}
