package pcd.lab09.actors.stash;

import akka.actor.AbstractActorWithStash;

public class ActorWithProtocol extends AbstractActorWithStash {
	@Override
	public Receive createReceive() {
		return receiveBuilder()
		.matchEquals("open", s -> {
			System.out.println("opened.");
			getContext().become(receiveBuilder()
				.matchEquals("write", ws -> {
					System.out.println("do write.");
				})
				.matchEquals("close", cs -> {
					System.out.println("close.");
					unstashAll();
					getContext().unbecome();
				})
				.matchAny(msg -> stash()).build(), false);
		})
		.matchAny(msg -> stash())
		.build();
	}
}
