package pcd.lab04.gui.chrono;

public interface CounterEventListener {
	void counterChanged(CounterEvent ev);
}
