package pcd.lab05.monitors.cyclic_barrier;

public interface Barrier {

	void hitAndWaitAll() throws InterruptedException;

}
